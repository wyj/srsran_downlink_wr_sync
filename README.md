UE: moran eNB: ustar  Attacker: ebc   attack subframe: 8    normal subframes: 7 shown subframes: 8
1sample = 170ns
| ue gain | enb gain | attack gain | timing offset(ns) | filename | image | estimated delay(ns) |
| ------ | ------ |------ |------ |------ |------ |------ |
|   67 | 60  | 89 | 500 |[moran1](https://gitlab.flux.utah.edu/wyj/wr_sync/-/blob/main/iq_bin/meu1.txt) | <img src="/images/meu1.png?raw=true" width="50" height="50"> |178|
|   67 | 60  | 85 | 500 |[moran1](https://gitlab.flux.utah.edu/wyj/wr_sync/-/blob/main/iq_bin/meu2.txt) | <img src="/images/meu2.png?raw=true" width="50" height="50"> | 115.7|
|   67 | 60  | 80 | 500 |[moran1](https://gitlab.flux.utah.edu/wyj/wr_sync/-/blob/main/iq_bin/meu3.txt) | <img src="/images/meu3.png?raw=true" width="50" height="50"> |52.6|
|   67 | 60  | 70 | 500 |[moran1](https://gitlab.flux.utah.edu/wyj/wr_sync/-/blob/main/iq_bin/meu4.txt) | <img src="/images/meu4.png?raw=true" width="50" height="50"> |247.79|
|   67 | 60  | 60 | 500 |[moran1](https://gitlab.flux.utah.edu/wyj/wr_sync/-/blob/main/iq_bin/meu5.txt) | <img src="/images/meu5.png?raw=true" width="50" height="50"> |384.5|
|   67 | 60  | 80 | 1000 |[moran1](https://gitlab.flux.utah.edu/wyj/wr_sync/-/blob/main/iq_bin/meu6.txt) | <img src="/images/meu6.png?raw=true" width="50" height="50"> |90.4|
|   67 | 60  | 80 | 100 |[moran1](https://gitlab.flux.utah.edu/wyj/wr_sync/-/blob/main/iq_bin/meu7.txt) | <img src="/images/meu7.png?raw=true" width="50" height="50"> |67.8|
|   67 | 60  | 75 | 100 |[moran1](https://gitlab.flux.utah.edu/wyj/wr_sync/-/blob/main/iq_bin/meu8.txt) | <img src="/images/meu8.png?raw=true" width="50" height="50"> |163.8|
|   67 | 55  | 89 | 5000 |[moran1](https://gitlab.flux.utah.edu/wyj/wr_sync/-/blob/main/iq_bin/meu9.txt) | <img src="/images/meu9.png?raw=true" width="50" height="50"> |524.3|


# Problem 2: localization error'

good example: meu7

<img src="/images/m7.png?raw=true" width="800" height="300">

bad example: meu5

<img src="/images/m5.png?raw=true" width="800" height="300">

solution: do detection first

out of ability: meu9

<img src="/images/m9.png?raw=true" width="800" height="300">

resolution problem




UE: moran eNB:ebc Attacker: ustar   attack subframes:1,2,3,4 normal subframe: 0 shown subframes: 1

| ue gain | enb gain | attack gain | timing offset | filename | image |
| ------ | ------ |------ |------ |------ |------ |
|   67 | 89  | 80 | 0.0001 |[moran1](https://gitlab.flux.utah.edu/wyj/wr_sync/-/blob/main/iq_bin/moran1.txt) | <img src="/images/moran1.png?raw=true" width="50" height="50"> |
|   67 | 89  | 70 | 0.0001 |[moran1](https://gitlab.flux.utah.edu/wyj/wr_sync/-/blob/main/iq_bin/moran2.txt) | <img src="/images/moran2.png?raw=true" width="50" height="50"> |
|   67 | 89  | 60 | 0.0001 |[moran1](https://gitlab.flux.utah.edu/wyj/wr_sync/-/blob/main/iq_bin/moran3.txt) | <img src="/images/moran3.png?raw=true" width="50" height="50"> |
|   67 | 89  | 65 | 0.0001 |[moran1](https://gitlab.flux.utah.edu/wyj/wr_sync/-/blob/main/iq_bin/moran4.txt) | <img src="/images/moran4.png?raw=true" width="50" height="50"> |
|   67 | 89  | 75 | 0.0001 |[moran1](https://gitlab.flux.utah.edu/wyj/wr_sync/-/blob/main/iq_bin/moran5.txt) | <img src="/images/moran5.png?raw=true" width="50" height="50"> |
|   67 | 89  | 85 | 0.0001 |[moran1](https://gitlab.flux.utah.edu/wyj/wr_sync/-/blob/main/iq_bin/moran6.txt) | <img src="/images/moran6.png?raw=true" width="50" height="50"> |
|   67 | 89  | 89 | 0.0001 |[moran1](https://gitlab.flux.utah.edu/wyj/wr_sync/-/blob/main/iq_bin/moran7.txt) | <img src="/images/moran7.png?raw=true" width="50" height="50"> |
|   67 | 89  | 80 | 0.00001 |[moran1](https://gitlab.flux.utah.edu/wyj/wr_sync/-/blob/main/iq_bin/moran8.txt) | <img src="/images/moran8.png?raw=true" width="50" height="50"> |
|   67 | 89  | 85 | 0.00001 |[moran1](https://gitlab.flux.utah.edu/wyj/wr_sync/-/blob/main/iq_bin/moran9.txt) | <img src="/images/moran9.png?raw=true" width="50" height="50"> |
|   67 | 89  | 75 | 0.00001 |[moran1](https://gitlab.flux.utah.edu/wyj/wr_sync/-/blob/main/iq_bin/moran10.txt) | <img src="/images/moran10.png?raw=true" width="50" height="50"> |
|   67 | 89  | 70 | 0.00001 |[moran1](https://gitlab.flux.utah.edu/wyj/wr_sync/-/blob/main/iq_bin/moran11.txt) | <img src="/images/moran11.png?raw=true" width="50" height="50"> |
|   67 | 89  | 65 | 0.00001 |[moran1](https://gitlab.flux.utah.edu/wyj/wr_sync/-/blob/main/iq_bin/moran12.txt) | <img src="/images/moran12.png?raw=true" width="50" height="50"> |
|   67 | 89  | 70 | 0.000001 |[moran1](https://gitlab.flux.utah.edu/wyj/wr_sync/-/blob/main/iq_bin/moran13.txt) | <img src="/images/moran13.png?raw=true" width="50" height="50"> |
|   67 | 89  | 80 | 0.000001 |[moran1](https://gitlab.flux.utah.edu/wyj/wr_sync/-/blob/main/iq_bin/moran14.txt) | <img src="/images/moran14.png?raw=true" width="50" height="50"> |
|   67 | 89  | 80 | 0.0000001 |[moran1](https://gitlab.flux.utah.edu/wyj/wr_sync/-/blob/main/iq_bin/moran15.txt) | <img src="/images/moran15.png?raw=true" width="50" height="50"> |

